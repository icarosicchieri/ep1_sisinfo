+++
title = 'Minha Biografia'
date = 2023-09-27T09:15:36-03:00
draft = false
+++

Se você quiser ver minhas informações de uma maneira extremamente simples e entediante, clique [aqui](/ep1_sisinfo/about_list/). Agora se está disposto a saber um pouco mais, e a perceber que, além do que eu vou contar, eu também sou um péssimo escritor, leia o textinho a seguir!

---

Meu nome é Ícaro Moro Sicchieri, eu tenho 20 anos e pretendo fazer muitos ainda =). Eu nasci em Sertãozinho-SP, assim como todos os membros que conheço da minha família, mas sempre morei em Ribeirão Preto-SP (Metrópole!). Eu sempre tive muito interesse por exatas e por esportes, logo, desde que me conheço por gente pratico as duas coisas, sendo a primeira por meio de jogos de lógicas, contagem de filas e outras diversões de uma criança nerd. Já quanto aos esportes, fiz futebol desde cedo, mas o que realmente me apaixonei foi o tênis, que esporte lindo!

Em Ribeirão estudei até o quinto ano na Escola do Amanhã, depois fiz COC até o nono, e, por fim estudei no Colégio FAAP durante o ensino médio, vulgo melhor colégio da região. Quando eu pensava na faculdade eu só sabia que queria fazer algo de exatas, e que não queria ser professor (enfim, a timidez), logo decidi que faria engenharia! Agora só faltava escolher o curso (que bom que só havia 76239623 opções). Os meus pretendentes eram Mecânica, Elétrica e Civil (uma época quis fazer Materiais, mas ainda bem que mudei de ideia), mas logo antes de me inscrever na Fuvest, um amigo meu me apresentou a Mecatrônica, e como ela juntava grande parte do que eu tinha interesse, essa foi minha escolha final.

Saindo um pouco da ordem cronológica, gostaria de falar um pouco sobre o que eu faço no meu tempo livre. Fazendo jus àquela criança que sempre gostou de jogos de lógica, eu gosto muito de jogar xadrez e campo minado, tinha uma época que até tentava bater o recorde de um site nesse jogo. Ah, se quiser o link ta [aí](https://rachacuca.com.br/jogos/campo-minado/). Além disso, eu também gosto de fazer origamis, e hora ou outra eu pego qualquer objeto dobrável na minha frente e começo a fazer tsurus, e às vezes eu faço umas bolinhas fofas que dão trabalho. Enfim, também curto ouvir música, ver séries, filmes... ou seja, aquela coisa padrão que todos devem se orgulhar.

Voltando para a cronologia, e tentando (sem sucesso) ser um pouco mais sério, vou falar sobre minha jornada até agora na faculdade. O meu primeiro ano fiz em Ribeirão, então não sofri muitos problemas de adaptação, já que estávamos em pandemia, então o curso era EAD. No segundo ano, tive que me mudar pra São Paulo, e aí sim eu descobri o que era uma metrópole! No começo foi estranho para me acostumar e pegar o ritmo, mas agora sou viciado e até acho RP um pouco parado demais. Já quanto a Poli, o segundo ano foi meio corrido, por não ter o conforto do EAD, mas consegui me virar bem e serviu para mostrar que eu não faria nada diferente de Mecatrônica. E, também surgiu uma nova parte crucial: os treinos de tênis!. Eu acabei largando o grupo de extensão que eu tava (Skyrats), mas foi por outros 500.

Agora, o terceiro ano está um pouquinho mais bagunçado, mas continuo gostando bastante. No momento estou fazendo uma IC com a professora Larissa, sobre como controlar o robô KUKA (Um braço mecânico incrível que fica no prédio da Mecânica) por meio do Aprendizado por Reforço (enfim mergulhando no mundo bonito e confuso das IAs). E, também sou tesoureiro do CAM (Centro Acadêmico da Mecânica e da Mecatrônica), cargo que me ajudou a ter uma maturidade e responsabilidade maiores, apesar de eu ainda ter muito a desenvolver. Além disso prestei para DD no meio desse ano, e acabei passando para Turim, cidade da FIAT! No entanto, ainda não tenho certeza absoluta se vou, é uma decisão bem difícil...

---

Enfim, acho que eu falei até demais, mas se você leu até aqui, com certeza merece informações confidenciais, então aí estão, são todas suas [!](/ep1_sisinfo/curiosidade/)

