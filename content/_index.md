Seja muito bem-vindo(a) ao meu primeiro site!

Aqui você encontrará algumas informações muito interessantes sobre mim, como por exemplo:


- Minha biografia
- Curiosidades sobre mim
- Minhas preferências (rankings)


Sinta-se à vontade para navegar pelo site e descobrir um pouco mais sobre como eu enxergo o mundo, ou melhor, como eu "rankeio" ele!