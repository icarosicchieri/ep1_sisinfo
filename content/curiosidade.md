+++
title = 'Curiosidades '
date = 2023-09-30T22:59:30-03:00
draft = false
+++

Muito bem, você chegou nas curiosidade surpresas. Não consegui pensar em muitas mas era só pra ter algo diferente, então vamos lá!

---

- Quebrei o braço quando era criança e hoje ele é torto porque o cotovelo não cicatrizou no lugar certo
- Eu amo muito mitologia, e às vezes fico pensando se a origem do meu nome tem algo a ver com eu gostar tanto
- Quando eu era mais novo eu pensava que eu tinha fobofobia, que é o medo de sentir de sentir medo, mas acho que eu só era medroso demais mesmo kkkkkkkk
- A origem do meu nome vem da música [Plano Diabólico](https://www.youtube.com/watch?v=Y0MNnD3f404) do Língua de Trapo. Meu pai ama essa banda e odeia quando acham que a origem vem da música do Byafra (nem merece o [link](https://docs.google.com/document/d/142IVR59EMxoEaJm-Kq85_7bUrWXFUWFxjvib3W7x_hQ/edit?usp=sharing))