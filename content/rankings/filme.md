+++
title = 'Filmes'
date = 2023-10-01T01:05:54-03:00
draft = false
+++

Tem **MUITOS** filmes que eu preciso ver ainda, mas segue a lista dos que vi e me lembro

---
1. A Origem (Inception)

2. Até o Último Homem

3. A Vida é Bela

4. Amnesia

5. As Duas Faces de um Crime

6. Interestelar

7. Whiplash

---

SIM, eu gosto muito do Nolan. Tinha mais filme pra entrar, mas é bem difícil rankear

>[voltar](/ep1_sisinfo/rankings/)