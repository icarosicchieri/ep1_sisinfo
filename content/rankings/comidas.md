+++
title = 'Comidas'
date = 2023-10-01T13:55:56-03:00
draft = false
+++

Quem não ama comer, não é mesmo?

---
1. Estrogonofe de Carne

2. Hambúrger

3. Escondidinho de Carne Seca

4. Bisteca Acebolada

5. Filé a Parmegiana

6. Lasanha

7. Pizza

---

>[voltar](/ep1_sisinfo/rankings/)