+++
title = 'Minha Biografia'
date = 2023-09-30T21:44:04-03:00
draft = false
+++

Ok, você tem preguiça mesmo... Então segue a lista!

---

**Nome:** Ícaro Moro Sicchieri

**Idade:** 20 anos

**Data da Nascimento:**: 12 de Abril de 2003

**Local de Nascimento:** Sertãozinho-SP

**Ensino Médio:** Colégio FAAP - Ribeirão Preto

**Faculdade:** Escola Politécnica da USP

**Curso:** Mecatrônica

**Mãe:** Renata Moro Sicchieri

**Pai:** Alberto Marcelo Sicchieri

**Esportes:** Tênis, vôlei e futebol, mas principalmente tênis

**Hobbies:** Jogar tênis (sim, eu gosto muito), ver séries, fazer origami (na real só sei fazer tsuru), jogar campo minado e outros jogos de lógica, e ouvir música

---

Vou te dar mais uma chance, vai. O textinho está [aqui](/ep1_sisinfo/about/).

